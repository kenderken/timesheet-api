from django.forms import widgets
from rest_framework import serializers
from models import Timeentry, Task
from django.contrib.auth.models import User

class TimeentrySerializer ( serializers.ModelSerializer ):
    task = serializers.PrimaryKeyRelatedField ()
    task_name = serializers.RelatedField ( source = "task" )
    time_spent = serializers.Field ( source = "time_spent" )
    class Meta:
        model = Timeentry
        fields = ( 'id', 'task', 'time_start', 'time_end', 'time_spent', 'task_name', 'total_time' )

class TaskSerializer ( serializers.ModelSerializer ):
    # entries = TimeentrySerializer ( many = True )
    total_time_seconds = serializers.Field ( source = "total_time_spent" )
    has_unended_entries = serializers.Field ( source = "has_unended_entries" )
    is_old_task = serializers.Field ( source = "is_old_task" )

    hide_from_report = serializers.BooleanField (source = 'hide_from_report')
    class Meta:
        model = Task
        # exclude = ('is_hide_from_report', )
        
class UserSerializer ( serializers.ModelSerializer ):
    tasks = serializers.PrimaryKeyRelatedField ( many=True )
    
    class Meta:
        model = User
        fields = ( 'id', 'username', 'tasks', )

class TaskAndTimeSerializer ( serializers.Serializer ):
    task = TaskSerializer ()
    total_time = serializers.Field () 
    running_time = serializers.Field ()
    
    
class TaskDailySerializer ( serializers.Serializer ):
    date = serializers.Field ()
    tasks = TaskAndTimeSerializer ( many = True )
    total_time = serializers.Field ()
    running_time = serializers.Field ()
    
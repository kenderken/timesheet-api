from django.conf.urls import patterns, include, url
from rest_framework.urlpatterns import format_suffix_patterns
import views

urlpatterns = patterns('views',
    # url(r'^users/$', views.UserList.as_view()),
    # url(r'^users/(?P<pk>[0-9]+)/$', views.UserInstance.as_view()),
    url(r'^tasks/(?P<pk>[0-9]+)/$', views.TaskDetail.as_view()),
    url(r'^tasks/$', views.TaskList.as_view()),
    # url(r'^entries/(?P<pk>[0-9]+)/$', views.EntryDetail.as_view()),
    # url(r'^entries/$', views.EntryList.as_view()),
    
    url(r'^tasks/newentry/(?P<name>.*)', views.TaskWithEntryCreation.as_view()),
    url(r'^tasks/(?P<pk>[0-9]+)/newentry/$', views.TaskWithEntryCreation.as_view()),
    url(r'^tasks/(?P<pk>[0-9]+)/finish/$', views.TaskFinish.as_view()),
    url(r'^tasks/(?P<pk>[0-9]+)/hide/(?P<hide>\d)/$', views.TaskHide.as_view()),
    url(r'^entries/(?P<pk>[0-9]+)/finish/$', views.EntryFinish.as_view()),
    
    url(r'^tasks/by_day/$', views.ReportTasksByDayByTime.as_view(), name = "report_by_day" ),
    
)

# urlpatterns = format_suffix_patterns(urlpatterns, allowed = ["json", "pdf"])

urlpatterns += patterns('',
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
)

urlpatterns += patterns('',
    url(r'^api-token-auth/', 'rest_framework.authtoken.views.obtain_auth_token')
)

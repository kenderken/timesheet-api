Number.prototype.toHHMMSS = function () {
    var seconds = Math.floor(this),
        hours = Math.floor(seconds / 3600);
    seconds -= hours*3600;
    var minutes = Math.floor(seconds / 60);
    seconds -= minutes*60;

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours+':'+minutes+':'+seconds;
}


function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
}


$(document).ready ( function (){
    var update_interval = 2;
    var reload_interval = 600;
    var timersArray = [];
    
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    
    function _focus ()
    {
        
        $("#title").focus();
        $("#title").select ();
    }
    function updateTimes ( id ) 
    {
        var h = $("#hiddentime-"+id).html();
        var f = parseFloat(h);
        f = f + update_interval;
        $("#time-"+id).html ( f.toHHMMSS() + "");
        $("#hiddentime-"+id).html ( f );
    }
        
    function get_tasks_list ()
    {
        _focus ();
        for (i = 0; i < timersArray.length; i++) {
            clearInterval ( timersArray [ i ]);
        }
        timersArray = [];
        var url = "/api/v1/tasks/";
        $.ajax (url).done(function (data) {
            var html = "";
            // alert ( data.results );
            $.each(data.results, function(i, item) {
                if (item.has_unended_entries) {
                    html = html + "<tr class='active'><td class='ajaxlink'>";
                } else {
                    html = html + "<tr><td class='ajaxlink'>";
                }
                    
                if (item.has_unended_entries) {
                    html = html + "<span class='ajaxlinkstop' href='/api/v1/tasks/"+item.id+"/finish/'>stop</span>";
                    i = setInterval ( function () {
                        updateTimes ( item.id );
                    }, update_interval * 1000); 
                    timersArray [ timersArray.length ] = i;
                } else {
                    html = html + "<span class='ajaxlinkstart' href='/api/v1/tasks/"+item.id+"/newentry/?__force_finish=1'>start</span>";
                        
                }
                    
                html = html + "</td><td class='old_"+item.is_old_task+" hide_"+item.hide_from_report+"'>"+item.title+" (#"+item.id+")</td><td>";
                html = html + "<span class='hidden' id='hiddentime-"+item.id+"'>"+item.total_time_seconds+"</span><td class='time' id='time-"+item.id+"'>"+item.total_time_seconds.toHHMMSS()+" </td><td>";
                html = html + "</td>";
                html = html + "<td class='util'>";
                // html = html + "<span class='ajaxlinkdel' href='/api/v1/tasks/" + item.id + "/'>delete</span>";
                if (item.hide_from_report) {
                    html = html + " <span class='ajaxlinkhide' href='/api/v1/tasks/" + item.id + "/hide/0/'>show</span></td>";
                } else {
                    html = html + " <span class='ajaxlinkhide' href='/api/v1/tasks/" + item.id + "/hide/1/'>hide</span></td>";
                }
                
                html = html + "</tr>";
                // alert(item.title);
            });
            $("#tasks").html(html);
                
            $(".ajaxlinkstop").click ( function () {
                // alert($(this).attr("href"));
                $.post ($(this).attr("href")).done (function (data) {
                    get_tasks_list ();
                });
                return false;
            });
            $(".ajaxlinkstart").click ( function () {
                // alert($(this).attr("href"));
                $.post ($(this).attr("href")).done (function (data) {
                    get_tasks_list ();
                });
                return false;
            });
            $(".ajaxlinkdel").click ( function () {
                // alert($(this).attr("href"));
                if (confirm('Are you sure you want to delete this?')) {
                    $.ajax ({
                        type: "DELETE",
                        url: $(this).attr("href")
                    }).done (function (data) {
                        get_tasks_list ();
                    });
                } else {
                    _focus ();
                }
                return false;
            });
            $(".ajaxlinkhide").click ( function () {
                // alert($(this).attr("href"));
                $.ajax ({
                    type: "POST",
                    url: $(this).attr("href"),
                    data: {is_hide_from_report: 'true'}
                }).done (function (data) {
                    get_tasks_list ();
                });
                return false;
            });
                
           // alert(data); 
        });
    }
        
        
    $("#taskentry").submit ( function () {
        var url = "/api/v1/tasks/newentry/" + encodeURIComponent($("#title").val());
        $.post(url).done ( function (data) {
            get_tasks_list();
        });
        return false;
    });
    get_tasks_list();
    setInterval ( get_tasks_list, reload_interval * 1000 )
});

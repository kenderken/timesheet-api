import datetime

from django.db import models
from django.utils.timezone import now

from django.contrib import admin
# Create your models here.

MIN_TIMEENTRY_TIME = 5

class Task ( models.Model ):
    title = models.CharField ( null = True, blank = True, max_length = 256 )
    owner = models.ForeignKey( 'auth.User', related_name = 'tasks', editable = False , blank = True , null = True )
    is_hide_from_report = models.BooleanField ( default = 0, editable = True )
    
    num_unfinished_entries = models.IntegerField ( default = 0, editable = False )
    
    def hide_from_report (self):
        return self.is_hide_from_report == True
        
    def __unicode__ (self):
        return u'%s' % self.title
        
    def latest_changed_entry ( self ):
        ents = self.entries.order_by('time_end', 'time_start')
        return ents[0]
        
    def total_time_spent (self ):
        tt = 0
        for e in self.entries.all(): #.exclude(time_end = None):
            tt += e.time_spent()
        return int(tt)
        
    def has_unended_entries ( self ):
        return self.entries.filter(time_end = None).count() != 0
        
    def is_old_task ( self ):
        ens = self.entries.order_by ( '-time_start' )
        if ens.count() > 0:
            if (now () - ens[0].time_start) > datetime.timedelta ( 1 ):
                return True
            else:
                return False
        else:
            return True
        
    def save ( self ):
        ens = self.entries.filter(time_end = None)
        self.num_unfinished_entries = ens.count()
        print "saving task, ", self.title, self.num_unfinished_entries, "isHidden: ", self.is_hide_from_report
        super ( Task, self ).save()
    
class Timeentry ( models.Model ):
    task = models.ForeignKey ( Task, related_name = 'entries' )
    time_start = models.DateTimeField ( editable = True )
    time_end = models.DateTimeField ( null = True, blank = True, default = None )
    isFlagged = models.BooleanField ( default = False )
    total_time = models.IntegerField ( default = 0, editable = False )
    
    def time_spent (self):
        if self.total_time:
            return self.total_time
        else:
            return (now() - self.time_start).total_seconds()
    
    def __unicode__ (self):
        return u'Entry started: %s, ended: %s (task: %s)' % (self.time_start, self.time_end, self.task)
    
    def finish(self):
        if self.time_end == None:
            self.time_end = now()
            if self.time_spent() < MIN_TIMEENTRY_TIME:
                print "Deleting too short time entry", self.time_spent()
                t = self.task
                self.delete()
                t.save()
                return False
            self.total_time = (self.time_end - self.time_start).total_seconds()
            self.save()
            return True
        else:
            return False
    
    def save ( self ):
        if self.id == None and self.time_start == None:
            self.time_start = now()
            pass
            
        if self.time_end and self.time_start:
            self.total_time = (self.time_end - self.time_start).total_seconds()
            
        super ( Timeentry, self ).save()
        self.task.save()
        

admin.site.register ( Timeentry )
admin.site.register ( Task )
# Create your views here.

import datetime

from models import Task, Timeentry
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, render_to_response
from django.http import Http404
from django.utils.timezone import now, make_aware, get_current_timezone, localtime
from django.db.models import *
from django.template import RequestContext
#from django.db.models import exceptions

from serializers import TaskSerializer, TimeentrySerializer, UserSerializer, TaskDailySerializer

from rest_framework import mixins
from rest_framework import generics
from rest_framework import permissions
from rest_framework import status

from rest_framework.views import APIView
from rest_framework.response import Response

import templatetags.timefilter

class AttributeDict(dict): 
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__

#######
# permissions
#######

class IsTaskOwner ( permissions.BasePermission ):
    def has_object_permission ( self, request, view, obj ):
        print ("Is owner? ", obj.owner)
        return obj.owner == request.user

class IsEntryOwner ( permissions.BasePermission ):
    def has_object_permission ( self, request, view, obj ):
        return obj.task.owner == request.user
        

class UserList ( generics.ListAPIView ):
    model = User
    serializer_class = UserSerializer
    permission_classes = ( permissions.IsAuthenticated, )
    
class UserInstance ( generics.RetrieveAPIView ):
    model = User
    serializer_class = UserSerializer
    
class TaskList ( generics.ListCreateAPIView ):
    model = Task
    serializer_class = TaskSerializer
    permission_classes = ( permissions.IsAuthenticated, )
    paginate_by = 10
    paginate_by_param = 'page_size'
            
    def get_queryset ( self ):
        qs = super ( TaskList, self ).get_queryset ().annotate (last_entry=Max('entries__time_end'))
        # print qs, self.request.user.is_authenticated()
        return qs.filter ( owner = self.request.user ).order_by ('-num_unfinished_entries', '-last_entry')
                    
    def pre_save(self, obj):
        obj.owner = self.request.user
    
class TaskDetail ( generics.RetrieveUpdateDestroyAPIView ) :
    model = Task
    serializer_class = TaskSerializer
    permission_classes = ( IsTaskOwner, )
    
    def pre_save(self, obj):
        obj.owner = self.request.user
        
    def put ( self, *args, **kwargs ):
        print self, args, kwargs, self.request.DATA
        
        return super (TaskDetail, self).put(*args, **kwargs)
        
class EntryDetail ( generics.RetrieveUpdateDestroyAPIView ):
    model = Timeentry
    serializer_class = TimeentrySerializer
    permission_classes = ( IsEntryOwner, )

class EntryList ( generics.ListCreateAPIView ):
    model = Timeentry
    serializer_class = TimeentrySerializer
    permission_classes = ( permissions.IsAuthenticated, )
      

class TaskWithEntryCreation ( APIView ):
    # permission_classes = (permissions.IsAuthenticated, )
    
    def post ( self, *args, **kwargs ):
        # new task
        print self.request.GET.get('__force_finish', 0), kwargs, args
        if self.request.GET.get('__force_finish', 0) == '1':
            # force to finish all this user's entries now
            print ("Finish forced")
            for e in Timeentry.objects.filter(task__owner = self.request.user).filter(time_end = None):
                print ("finishing: ", e)
                e.finish()
                
        if kwargs.has_key ( 'pk' ):
            t = get_object_or_404 ( Task, pk = kwargs [ 'pk' ])
        else:
            t = Task ( owner = self.request.user, title = kwargs [ 'name' ])
            t.save()
        # new entry
        te = Timeentry ( task = t )
        te.save()
        
        s = TaskSerializer ( t )
        s.data["new_entry"] = te.pk
        return Response ( s.data )

class EntryFinish ( APIView ):
    permission_classes = (IsEntryOwner, )
    
    def post ( self, *args, **kwargs ):
        e = get_object_or_404 ( Timeentry, pk=kwargs [ 'pk' ] )
        e.finish()

        s = TimeentrySerializer ( e )
        return Response ( s.data )

class TaskFinish ( APIView ):
    
    permission_classes = (IsTaskOwner, )
    
    def post (self, *args, **kwargs):
        t = get_object_or_404 ( Task, pk = kwargs['pk' ])
        
        # get oldest started Entry for this task
        ents = t.entries.filter(time_end = None).order_by ( 'time_start' )
        if ents.count() == 0:
            raise Http404
        else:
            print (ents)
            e = ents[0]
            e.finish()
            
            return Response ( TaskSerializer ( t ).data )
            
class TaskHide ( APIView ):
    
    permission_classes = (IsTaskOwner, )
    
    def post (self, *args, **kwargs):
        t = get_object_or_404 ( Task, pk = kwargs['pk' ])
        print (kwargs)
        if int(kwargs ['hide']) == True:
            t.is_hide_from_report = True
        else:
            t.is_hide_from_report = False
        t.save()            
        return Response ( TaskSerializer ( t ).data )
            
# Reports
class ReportTasksByDayByTime ( APIView ):
    permission_classes = ( permissions.IsAuthenticated, )
            
    def get ( self, *args, **kwargs ):
        
        qs = Timeentry.objects.filter ( task__owner = self.request.user ).exclude ( isFlagged = True ).exclude ( task__is_hide_from_report = True )
        
        ds = self.request.GET.get('__date_start', None)
        de = self.request.GET.get('__date_end', None)
        if ds:
            ds = datetime.datetime.strptime ( ds, '%Y-%m-%d' )
            ds = make_aware ( ds, get_current_timezone ())
            try:
                qs = qs.filter ( time_end__gte=ds)
            except exceptions.ValidationError:
                ds = None
                pass
        if de:
            de = de + " 23:59:59"
            de = datetime.datetime.strptime ( de, '%Y-%m-%d %H:%M:%S' )
            print de
            de = make_aware ( de, get_current_timezone ())
            try:
                qs = qs.filter ( time_end__lte=de)
            except exceptions.ValidationError:
                de = None
                pass

        res = []
        days_map = {}
        tasks_map = {}
        day_total_time = {}
        all_time = 0
        running_time = 0
        for q in qs.annotate(empty_time=Count('time_end')).order_by('-empty_time', 'time_end'):
            te = None

            if q.time_end:
                te = q.time_end
            else:
                te = q.time_start
                
            date_str = localtime(te).date().strftime("%Y-%m-%d")
            task_id = q.task.id
            task_key = "%s_%s" % (date_str, task_id)
            if not days_map.has_key ( date_str ):
                d = AttributeDict({"date" : date_str, "tasks": [], "total_time" : 0, "running_time" : 0})
                res.append (d)
                days_map [ date_str ] = len(res)-1
            else:
                d = res[days_map[date_str]]
            if not tasks_map.has_key ( task_key ):
                t = AttributeDict({"task" : q.task, "total_time" : 0, "running_time" : 0})
                d["tasks"].append ( t )
                tasks_map [ task_key ] = len ( d["tasks"] )-1
            else:
                t = d["tasks"][tasks_map[task_key]]
            ts = q.time_spent()
            if (q.time_end):
                t["total_time"] += ts
                d["total_time"] += ts
                all_time += ts
            else:
                print "+++", ts
                t["running_time"] += ts
                d["running_time"] += ts
                print t["running_time"]
                running_time += ts
            
        ser = TaskDailySerializer ( res, many = True ).data
        if not ds:
            try:
                ds = ser[0]["date"]
                ds = datetime.datetime.strptime ( ds, '%Y-%m-%d' )
                ds = make_aware ( ds, get_current_timezone ())
                
            except IndexError:
                raise Http404
        if not de:
            try:
                de = ser[-1]["date"]
                de = datetime.datetime.strptime ( de, '%Y-%m-%d' )
                de = make_aware ( de, get_current_timezone ())
                
            except IndexError:
                raise Http404
        
        ser = { "report" : ser, "first_date" : ds, "last_date" : de, "all_time" : all_time, "running_time" : running_time }
        if self.request.GET.get ('__format', None ) == 'html':
            return render_to_response ( "report_by_day.html", ser, context_instance=RequestContext(self.request) )
            
        return Response ( ser )
        # print qs
        
        

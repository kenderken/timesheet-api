from django import template

register = template.Library()

@register.filter
def timefilter ( value ):
    seconds = int(value)
    hours = int(seconds/3600)
    seconds -= hours * 3600
    minutes = int(seconds / 60)
    seconds -= minutes * 60
    

    if (hours   < 10):
        hours   = "0%s" % hours
    if (minutes < 10):
        minutes = "0%s" % minutes
    if (seconds < 10):
        seconds = "0%s" % seconds
        
    return "%s:%s:%s" % (hours, minutes, seconds)
    
    